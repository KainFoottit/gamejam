﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropSpawner : MonoBehaviour {



    public GameObject suitcase1;
    public GameObject suitcase2;


    private void Start()
    {
        InvokeRepeating("SpawnSuitcase", 3, 8);
    }

    void SpawnSuitcase()
    {
        float objToSpawn = Random.value;

        if(objToSpawn <0.5f)
        {
            GameObject spawnedSuitcase = (GameObject)Instantiate(suitcase1,GameObject.Find("SuitcaseSpawnPoint").transform.position, Quaternion.identity);
        }
        else
        {
            GameObject spawnedSuitcase = (GameObject)Instantiate(suitcase2, GameObject.Find("SuitcaseSpawnPoint").transform.position, Quaternion.identity);
        }
        
    }


}
