﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkDestroyer : MonoBehaviour
{

    private float timeToDestroySelf;

    private void Update()
    {
        timeToDestroySelf += Time.deltaTime;


        if (timeToDestroySelf >= 20)
        {
            Destroy(this.gameObject);
        }
    }
}
