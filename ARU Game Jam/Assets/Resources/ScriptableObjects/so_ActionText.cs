﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.ScriptableObjects
{
    [CreateAssetMenu(fileName = "so_ActionText", menuName = "ActionText", order = 0)]
    public class so_ActionText : ScriptableObject
    {
        private string[] _actionText;
        public string[] actionText
        {
            get
            {
                if(_actionText == null)
                {
                    Debug.LogError("_actiontext in so_ActionText is null, have you forgotten to parse from the .txt");
                }
                return _actionText;
            }
        }

        public void SetText(string[] textFromDoc)
        {
            _actionText = textFromDoc;
        }
    }
}
