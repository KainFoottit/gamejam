﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpperBaggageSpawner : MonoBehaviour
{
    public GameObject baggageSpawn;
    public bool respawnCooldown = false;
    int[] spawnFrequency = new int[] {12, 14, 16 };

    GameObject bag1;
    GameObject bag2;

    void Update ()
    {
        if (!respawnCooldown)
        {
            SpawnBag();
        }
    }

    void SpawnBag()
    {
        bag1 = Resources.Load("Brown_Case") as GameObject;
        bag2 = Resources.Load("Silver_Case") as GameObject;



        StartCoroutine(RespawnCooldown());
    }

    IEnumerator RespawnCooldown()
    {
        respawnCooldown = true;
        yield return new WaitForSeconds(spawnFrequency[Random.Range(0, spawnFrequency.Length)]);
        respawnCooldown = false;
    }
}
