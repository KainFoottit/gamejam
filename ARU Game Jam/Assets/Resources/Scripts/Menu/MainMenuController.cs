﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public void ButtonClicked(Button buttonName)
    {
        string buttonNameScene = buttonName.name;
        ChangeScenes(buttonNameScene);
    }
    private void ChangeScenes(string buttonNameScene)
    {
        switch (buttonNameScene)
        {
            case "PlayButton":
                print(buttonNameScene + " was clicked.");
                SceneManager.LoadScene("_MainScene");
                break;

            case "OptionsButton":
                print(buttonNameScene + " was clicked.");
                SceneManager.LoadScene("_Options");
                break;

            case "CreditsButton":
                print(buttonNameScene + " was clicked.");
                SceneManager.LoadScene("_Credits");
                break;

            case "QuitButton":
                print(buttonNameScene + " was clicked.");
                Application.Quit();
                break;

            default:
                print("Error selecting scene to load. Menu has been loaded by default");
                SceneManager.LoadScene("_MainMenu");
                break;
        }
    }
}