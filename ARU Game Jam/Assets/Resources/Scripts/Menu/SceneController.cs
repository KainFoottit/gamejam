﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public void SceneLoader()
    {
        SceneManager.LoadScene("_MainScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}