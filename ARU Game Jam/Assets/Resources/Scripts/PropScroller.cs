﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropScroller : MonoBehaviour {

    [SerializeField]
    float scroll = 10f;

    private void Update()
    {
        Scroll();
    }

    private void Scroll()
    {
        gameObject.transform.Translate(new Vector3(-scroll * Time.timeScale, 0, 0));
    }
}
