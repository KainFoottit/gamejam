﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ConvoParse : MonoBehaviour
{

    private Text convoText;
    public TextAsset convoTextDoc;
    private string[] convoTextLines;
    public static int convoNumber = -1;

    private void Start()
    {
        convoText = GameObject.Find("Convo_Text").GetComponent<Text>();

        if (convoTextDoc != null)
        {
            convoTextLines = (convoTextDoc.text.Split('\n')); //Fills the array with the text from the text document
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //int eventNumber = Random.Range(0, eventTextLines.Length);
            convoNumber++;
            DisplayText(convoNumber);
        }
    }

    /// <summary>
    /// Runs this method with the eventNumber
    /// </summary>
    /// <param name="eventNumber"></param>
    public void DisplayText(int eventNumber)
    {
        convoText.text = convoTextLines[eventNumber];
    }
}
