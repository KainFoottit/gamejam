﻿using UnityEngine;

public class EventAudio : MonoBehaviour
{
    public AudioClip[] eventAudio;
    public AudioSource audioSource;

    void playEventAudio()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = eventAudio[EventParse.eventNumber];
        audioSource.Play();        
    }
}