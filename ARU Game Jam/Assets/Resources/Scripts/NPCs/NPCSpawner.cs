﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCSpawner : MonoBehaviour
{
    public GameObject NPCSpawn;
    public GameObject NPCSpawnUnique;

    public bool respawnCooldown = false;

    int[] spawnFrequency = new int[] { 8, 10, 12, 14 };

    int spawnCounter = 0;

    void Update()
    {
        if (!respawnCooldown)
        {
            if (spawnCounter == 4)
            {
                CreateUniqueNPC();
            }
            else
            {
                CreateNPC();
            }
        }
    }

    void CreateNPC()
    {
        GameObject npc = (GameObject)Instantiate(NPCSpawn, GameObject.Find("NPCSpawnPoint").transform.position, Quaternion.identity);
        npc.transform.parent = gameObject.transform;

        spawnCounter++;

        StartCoroutine(RespawnCooldown());
    }

    void CreateUniqueNPC()
    {
        GameObject npcUnique = (GameObject)Instantiate(NPCSpawnUnique, GameObject.Find("NPCSpawnPoint").transform.position, Quaternion.identity);
        npcUnique.transform.parent = gameObject.transform;

        spawnCounter -= 4;

        StartCoroutine(RespawnCooldown());
    }

    IEnumerator RespawnCooldown()
    {
        respawnCooldown = true;
        yield return new WaitForSeconds(spawnFrequency[Random.Range(0, spawnFrequency.Length)]);
        respawnCooldown = false;
    }
}