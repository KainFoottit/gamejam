﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UniqueNPCGenerator : MonoBehaviour
{
    [SerializeField]
    Sprite[] spritesBefore = new Sprite[14];
    [SerializeField]
    Sprite[] spritesAfter = new Sprite[14];

    public GameObject NPCSpawnUnique;
    public Sprite placeholderImage;

    public bool respawnCooldown = false;
    int[] spawnFrequency = new int[] { 8, 10, 12, 14 };

    void Start ()
    {
		
	}

    void Update()
    {
        if (EventTrigger.swapSprites)
        {
            for (int i = 0; i < spritesBefore.Length; i++ )
            {
                if (spritesBefore[i].Equals(spritesAfter[i]))
                {
                    spritesBefore[i] = spritesAfter[i];
                }
            }
        }
	}

    void CreateUniqueNPC()
    {
        placeholderImage = spritesBefore[Random.Range(0, spritesBefore.Length)];

        GameObject npcUnique = (GameObject)Instantiate(NPCSpawnUnique, GameObject.Find("NPCSpawnPoint").transform.position, Quaternion.identity);
        npcUnique.transform.parent = gameObject.transform;

        StartCoroutine(RespawnCooldown());
    }

    IEnumerator RespawnCooldown()
    {
        respawnCooldown = true;
        yield return new WaitForSeconds(spawnFrequency[Random.Range(0, spawnFrequency.Length)]);
        respawnCooldown = false;
    }
}