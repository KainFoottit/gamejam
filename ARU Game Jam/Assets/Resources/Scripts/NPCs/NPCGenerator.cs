﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCGenerator : MonoBehaviour
{
    [SerializeField]
    float scroll;

    private int scrollCount;

    [SerializeField]
    List<Sprite> headsList = new List<Sprite>();
    [SerializeField]
    List<Sprite> hairList = new List<Sprite>();
    [SerializeField]
    List<Sprite> hatList = new List<Sprite>();
    [SerializeField]
    List<Sprite> topList = new List<Sprite>();
    [SerializeField]
    List<Sprite> bottomList = new List<Sprite>();
    [SerializeField]
    List<Sprite> baggageList = new List<Sprite>();

    public Image head;
    public Image hair;
    public Image hat;
    public Image top;
    public Image bottom;
    public Image baggage;
    public Image convoText;

    public Sprite headSelected;
    public Sprite hairSelected;
    public Sprite hatSelected;
    public Sprite topSelected;
    public Sprite bottomSelected;
    public Sprite baggageSelected;

    Color[] clothesColours = new Color[6];
    Color[] hairColours = new Color[4];

    void Start()
    {
        clothesColours[0] = new Color32(229, 21, 154, 255);
        clothesColours[1] = new Color32(205, 170, 175, 255);
        clothesColours[2] = new Color32(166, 192, 212, 255);
        clothesColours[3] = new Color32(182, 165, 186, 255);

        hairColours[0] = Color.white;
        hairColours[1] = Color.black;
        hairColours[2] = Color.yellow;
        hairColours[3] = Color.red;

        ChooseParts();
    }

    public void Update()
    {
        Scroll();
        scrollCount++;
    }

    void ChooseParts()
    {
        headSelected = headsList[Random.Range(0, headsList.Count)];
        hairSelected = hairList[Random.Range(0, hairList.Count)];
        hatSelected = hatList[Random.Range(0, hatList.Count)];
        topSelected = topList[Random.Range(0, topList.Count)];
        bottomSelected = bottomList[Random.Range(0, bottomList.Count)];
        baggageSelected = baggageList[Random.Range(0, baggageList.Count)];

        head.GetComponent<Image>().sprite = headSelected;
        hair.GetComponent<Image>().sprite = hairSelected;
        hat.GetComponent<Image>().sprite = hatSelected;
        top.GetComponent<Image>().sprite = topSelected;
        bottom.GetComponent<Image>().sprite = bottomSelected;
        baggage.GetComponent<Image>().sprite = baggageSelected;

        hair.color = hairColours[Random.Range(0, hairColours.Length)];
        hat.color = clothesColours[Random.Range(0, clothesColours.Length)];
        top.color = clothesColours[Random.Range(0, clothesColours.Length)];
        bottom.color = clothesColours[Random.Range(0, clothesColours.Length)];
    }

    private void Scroll()
    {
        gameObject.transform.Translate(new Vector3(-scroll * Time.timeScale, 0, 0));
    }
}