﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QTEFlash : MonoBehaviour
{
    private Color32 flashColour = new Color32(255, 0, 0, 255);
    private Image qteImage;
    private Color32 defaultColour;

    private void Start()
    {
        qteImage = GetComponent<Image>();
        defaultColour = qteImage.color;

        //StartCoroutine(KeyFlash());
    }


    private void Update()
    {
        if(qteImage.enabled == true)
        {
            Invoke("KeyFlash", 0);
        }
    }
    private IEnumerator KeyFlash()
    {
        qteImage.color = flashColour;
        yield return new WaitForSeconds(1);
        qteImage.color = defaultColour;
    }
}
