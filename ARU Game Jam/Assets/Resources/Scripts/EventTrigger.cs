﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventTrigger : MonoBehaviour
{
    public static bool swapSprites;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("NPCUnique"))
        {
            swapSprites = true;
        }
            Debug.Log("Triggered");
    }
}