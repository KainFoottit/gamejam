﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventTimers : MonoBehaviour
{
    float qteCountDown = 10;
    QTEKeyGen QTEKeyGenScript;

    private void Start()
    {
        QTEKeyGenScript = GameObject.Find("QTEKeyGen_SH").GetComponent<QTEKeyGen>();
    }

    private void Update()
    {
        qteCountDown -= Time.deltaTime;

        if (qteCountDown <= 0)
        {
            QTEKeyGenScript.PickQTE();
            qteCountDown = 10;
        }
    }
}
