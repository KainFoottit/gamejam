﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QTEKeyGen : MonoBehaviour
{
    private string[] keyList; //List of possible keys to pick
    private Image keyImage; //The background of the key
    private Text keyText; //The text of the key
    private string keyString;

    private Transform imageTransform;
    private float randomX, randomY;

    bool qteOn = false;
    bool pickedCode = false;

    float qteTimer;

    private void Start()
    {
        keyImage = GameObject.Find("QTE_Key").GetComponent<Image>();
        keyText = GameObject.Find("QTE_Text").GetComponent<Text>();
        imageTransform = keyImage.transform;
        keyList = new string[8];

        if (keyImage.enabled == true)
        {
            keyImage.enabled = false;
            keyText.enabled = false;
        }

        randomX = Random.Range(-20, 20);
        randomY = Random.Range(-20, 20);
    }


    public void PickQTE()
    {
        if (pickedCode == false)
        {
            PickKey();
            pickedCode = true;
        }
        qteOn = true;
    }

    /// <summary>
    ///  1 =  J   5 =  T
    ///  2 =  L   6 =  M
    ///  3 =  B   7 =  P
    ///  4 =  K   8 =  E
    /// </summary>
    void PickKey()
    {
        int keyIndex = Random.Range(0, keyList.Length); //Picks a random key from the array

        switch (keyIndex)
        {
            case 0:
                keyString = "J";
                break;

            case 1:
                keyString = "L";
                break;

            case 2:
                keyString = "B";
                break;

            case 3:
                keyString = "K";
                break;

            case 4:
                keyString = "T";
                break;

            case 5:
                keyString = "M";
                break;

            case 6:
                keyString = "P";
                break;

            case 7:
                keyString = "E";
                break;

            default:
                Debug.Log("Can't convert");
                break;
        }

        keyText.text = keyString;
    }

    /// <summary>
    /// When a key is pressed it will convert the key to uppercase then check if its correct
    /// </summary>
    void CheckKeyPressed()
    {
        if (Input.anyKeyDown)
        {
            string playerKeyPressed = Input.inputString.ToUpper(); //Converts to uppercase

            if (playerKeyPressed != " ")
            {
                if (playerKeyPressed == keyString)
                {
                    print("Correct");
                    qteOn = false;
                }
                else if (playerKeyPressed != keyString)
                {
                    print("Wrong");
                    qteOn = false;
                }
                pickedCode = false;
            }
        }
    }

    private void Update()
    {
        if (qteOn == true)
        {
            imageTransform.position += new Vector3(randomX, randomY, 0) * Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.Space)) // This is for testing
        {
            if (pickedCode == false)
            {
                PickKey();
                pickedCode = true;
            }
            qteOn = true;
        }

        if (qteOn == true)
        {
            if (keyImage.enabled == false)
            {
                keyImage.enabled = true;
                keyText.enabled = true;
            }

            Time.timeScale = 0.3f;
            CheckKeyPressed();

            qteTimer += Time.deltaTime;
        }
        else
        {
            if (keyImage.enabled == true)
            {
                keyImage.enabled = false;
                keyText.enabled = false;
            }

            if(Time.timeScale != 1)
            {
                Time.timeScale = 1;
            }
        }

        if (qteTimer >= 5)
        {
            print("Player took to long");
            qteOn = false;
            qteTimer = 0;
        }
    }
}
