﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonColour : MonoBehaviour
{
    private Color32 defaultcolor = new Color32(255, 255, 255, 255);
    private Color32 clickedcolor = new Color32(176, 35, 35, 255);
    private Text textref;


    void Start()
    {
        textref = GetComponentInChildren<Text>();
    }

    public void ChangeColor()
    {
        if(textref.color == defaultcolor)
        {
            textref.color = clickedcolor;
        }
        else
        {
            textref.color = defaultcolor;
        }
    }
}
