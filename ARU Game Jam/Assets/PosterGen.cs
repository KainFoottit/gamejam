﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PosterGen : MonoBehaviour
{

    public List<Sprite> posterImagesList;
    private Image posterImage;

    private float random;

    private void Start()
    {
        posterImage = GetComponent<Image>();

        int index = Random.Range(0, posterImagesList.Count);

        posterImage.sprite = posterImagesList[index];


    }
}
